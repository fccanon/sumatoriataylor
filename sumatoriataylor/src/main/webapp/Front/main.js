$(function () {

    $('.validanumericos').keypress(function (e) {
        if (isNaN(this.value + String.fromCharCode(e.charCode)))
            return false;
    })
            .on("cut copy paste", function (e) {
                e.preventDefault();
            });

});

$('document').ready(function () {
    $("#formConsulta").bootstrapValidator({

        message: 'Este valor no es valido',

        feedbackIcons: {

            valid: 'glyphicon glyphicon-ok',

            invalid: 'glyphicon glyphicon-remove',

            validating: 'glyphicon glyphicon-refresh'

        },

        fields: {

            termino: {

                validators: {

                    notEmpty: {

                        message: 'El termino es requerido'

                    }

                }

            },

            grados: {

                validators: {

                    notEmpty: {

                        message: 'los grados son requerida'

                    }

                }

            }

        }

    });
})