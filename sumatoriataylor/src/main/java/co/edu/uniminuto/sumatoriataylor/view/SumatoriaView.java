/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uniminuto.sumatoriataylor.view;

import co.edu.uniminuto.sumatoriataylor.model.Factorial;
import co.edu.uniminuto.sumatoriataylor.model.Sumatoria;

/**
 *
 * @author Camilo Cañon
 * @version 1.0
 */
public class SumatoriaView {

    private static double valor;
    private static double suma;

    public SumatoriaView() {
        super();
        valor = 0L;
        suma = 0L;
    }

    /*
	 * Metodo que calcula la sumatoria de Taylor
	 * @param termino Valor de cuantos terminos se va hacer para la sumatoria de taylor
         * @param grados numero de grados a calcular para la sumatoria
	 * @return valor de la sumatoria 
	 * @version 1.0
     */
    public double calculoSumatoria(int termino, int grados) {
        Sumatoria sumatoria = new Sumatoria();
        for (int i = 0; i <= termino; i++) {
            valor = (sumatoria.getSigno() / (Factorial.factorial(2 * i + 1))) * (Math.pow(Math.toRadians(grados), (2 * i + 1)));
            sumatoria.setValor(valor);
            suma += valor;
            sumatoria.setSuma(suma);

        }

        return suma;

    }

}
