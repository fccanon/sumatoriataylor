/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uniminuto.sumatoriataylor.controller;

/**
 *
 * @author Camilo Cañon
 * @version 1.0
 */
import co.edu.uniminuto.sumatoriataylor.view.SumatoriaView;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ProductoController
 */
@WebServlet(description = "administra los datos de la sumatoria", urlPatterns = {"/sumatoria"})

public class SumatoriaController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SumatoriaController() {
        super();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head><meta charset=\"utf-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n"
                + "\n"
                + "        <!-- Bootstrap CSS -->\n"
                + "        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n"
                + "        <link rel=\"stylesheet\" href=\"/sumatoriataylor/Front/estilos.css\" type=\"text/css\"><link>\n"
                + "\n"
                + "        <!-- Optional JavaScript -->\n"
                + "        <!-- jQuery first, then Popper.js, then Bootstrap JS -->\n"
                + "        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n"
                + "        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n"
                + "        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n"
                + "</head>");
        out.println("<body id=\"bodyInicial\"><h1>Resultado Sumatoria de  Taylor</h1>\n"
                + "        <div class=\"container\">\n"
                + "            <div class=\"row\">");

        out.println("<form  id=\"resultado\" name=\"resultado\" >\n"
                + "                    <div class=\"form-group\">\n"
                + "                    <label>Termino:</label>");

        String term = request.getParameter("termino");
        out.println(term);
        out.println("</div>");
        out.println("<div class=\"form-group\">\n"
                + "                    <label>Grados:</label>");

        String grad = request.getParameter("grados");

        out.println(grad);
        out.println("</div>");
        int termino = Integer.parseInt(term);
        int grados = Integer.parseInt(grad);

        out.println("<div class=\"form-group\">\n"
                + "                    <label>Total Sumatoria Taylor:</label>");
        SumatoriaView sumatoriaView = new SumatoriaView();
        double total = sumatoriaView.calculoSumatoria(termino, grados);
        out.println(total);
        out.println("</div>");
        out.println("</div>\n"
                + "</div></body>");
        out.println("</html>");

    }
}
