package co.edu.uniminuto.sumatoriataylor.model;

/**
 * @author Camilo Cañon
 * @version 1.0
 */
public class Factorial {

    public Factorial() {
    }

    /*
	 * Metodo que calcula el factorial
	 * @param numero Valor de cuantos terminos se va hacer para la sumatoria de taylor
	 * @return factorial retorna el valor del factorial 
	 * @version 1.0
     */
    public static double factorial(int numero) {

        return (numero == 0 || numero == 1) ? 1 : numero * factorial(numero - 1);
    }

}
