package co.edu.uniminuto.sumatoriataylor.model;

/**
 * @author Camilo Cañon
 * @version 1.0
 *
 */
public class Sumatoria {

    public int signo = -1;
    public double suma;
    public double valor;

    public Sumatoria() {

    }

    public int getSigno() {
        return signo;
    }

    public void setSigno(int signo) {
        this.signo = signo;
    }

    public double getSuma() {
        return suma;
    }

    public void setSuma(double suma) {
        this.suma = suma;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

}
